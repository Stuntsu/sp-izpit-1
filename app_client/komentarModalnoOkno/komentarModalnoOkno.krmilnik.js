(function() {
  /* global angular */
  
  function komentarModalnoOkno($uibModalInstance, edugeocachePodatki, podrobnostiLokacije) {
    var vm = this;
    
    vm.podrobnostiLokacije = podrobnostiLokacije;
    
    vm.modalnoOkno = {
      zapri: function(odgovor) {
        $uibModalInstance.close(odgovor);
      },
      preklici: function() {
        $uibModalInstance.close();
      }
    };
    
    vm.dodajKomentar = function(idLokacije, podatkiObrazca) {
      edugeocachePodatki.dodajKomentarZaId(idLokacije, {
        naziv: podatkiObrazca.naziv,
        ocena: podatkiObrazca.ocena,
        komentar: podatkiObrazca.komentar
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
      return false;
    }
    
    vm.urediKomentar = function(idLokacije, podatkiObrazca, idKomentarja){
      //console.log("id je: " + idKomentarja);
      edugeocachePodatki.urediKomentarZaId(idLokacije, {
        
        naziv:  podatkiObrazca.naziv,
        ocena: podatkiObrazca.ocena,
        komentar: podatkiObrazca.komentar,
        idStarega: idKomentarja
        
      }).then(
        function success(odgovor) {
          vm.modalnoOkno.zapri(odgovor.data);
        },
        function error(odgovor) {
          vm.napakaNaObrazcu = "Napaka pri shranjevanju komentarja, poskusite znova!";
        }
      );
      return false;
      }
    
    vm.posiljanjePodatkov = function() {
      vm.napakaNaObrazcu = "";
      //console.log("urejanje: " + vm.podrobnostiLokacije.urejanje);
      
      if (!vm.podatkiObrazca.ocena || !vm.podatkiObrazca.komentar) {
        vm.napakaNaObrazcu = "Prosim, izpolnite vsa vnosna polja!";
        return false;
      } else {
        if(!vm.podrobnostiLokacije.urejanje){
          vm.dodajKomentar(vm.podrobnostiLokacije.idLokacije, vm.podatkiObrazca);
        }else{
          //console.log("Urejam!");
          vm.urediKomentar(vm.podrobnostiLokacije.idLokacije, vm.podatkiObrazca, vm.podrobnostiLokacije.idKomentarja);
        }
        
      }
    };
  }
  komentarModalnoOkno.$inject = ['$uibModalInstance', 'edugeocachePodatki', 'podrobnostiLokacije'];
  
  angular
    .module('edugeocache')
    .controller('komentarModalnoOkno', komentarModalnoOkno);
})();