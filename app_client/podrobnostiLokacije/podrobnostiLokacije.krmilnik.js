(function() {
  /* global angular */
  
  function podrobnostiLokacijeCtrl($routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija) {
    var vm = this;
    vm.idLokacije = $routeParams.idLokacije;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    vm.prvotnaStran = $location.path();
    
    
    
    var trenutniUporMail;
    var trenutniUporIme;
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        for(var i in vm.podatki.lokacija.komentarji){
          var trenutniUpor = vm.podatki.lokacija.komentarji[i].avtor;
          //console.log(trenutniUpor);
          if(trenutniUporIme == trenutniUpor){
            vm.podatki.lokacija.komentarji[i].avtorMail = true;
          }else{
             vm.podatki.lokacija.komentarji[i].avtorMail = false;
          }
        }
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
    
    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
    // EDITANJE KOMENTARJEV
    
    vm.urediTrenutniKomentar = function(id){
      console.log(id);
      var urejanje = true;
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv,
              urejanje: true,
              idKomentarja: id
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
        console.log(podatki);
        console.log(vm.podatki.lokacija.komentarji[3]);
          for(var i in vm.podatki.lokacija.komentarji){
            
            if(vm.podatki.lokacija.komentarji[i]._id == podatki._id){
              vm.podatki.lokacija.komentarji[i].ocena = podatki.ocena;
              vm.podatki.lokacija.komentarji[i].besediloKomentarja = podatki.besediloKomentarja;
              vm.podatki.lokacija.komentarji[i].datum = podatki.datum;
              vm.podatki.lokacija.komentarji[i].avtorMail = true;
            }
          }
          
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    }
    
      
    if(avtentikacija.jePrijavljen()){
      trenutniUporMail = avtentikacija.trenutniUporabnik().elektronskiNaslov;
      trenutniUporIme = avtentikacija.trenutniUporabnik().ime;
      
      
    }
    
    
  }
  podrobnostiLokacijeCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija'];
  
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();